#Extracts all threads from the futuretimeline.net forum and changes all links to point to this archive,
#not the forum itself, which will soon be defunct.
import requests
from bs4 import BeautifulSoup
import re
import os

#Gets the number of pages in a subforum or thread
def get_page_count(text):
     retval = -1
     try:
         retval = int(text.split("totalPages: ")[1].split(",")[0])
     except:
         retval = 1
     return retval
     
#Gets all pages in an individual thread and converts the links     
def parse_thread(thread_url):
    #Threads are stored in folders since they can have multiple pages.
    try:
        os.mkdir("forum/topic/"+thread_url.split("/")[-2])
    except:
        pass
    page_count = -1
    for i in range(1,9999999):
        print(thread_url)
        thread_text = requests.get(thread_url+"/page-%d"%i).text #html of each page of the thread
        soup = BeautifulSoup(thread_text,features="html5lib")
        if i == 1:
            page_count = get_page_count(thread_text)
        urls = soup.find_all("a") #all links from the page
        for url in urls:
            #if the url links to a forum or topic, replace it with a link to the corresponding forum or topic in this archive
            if "https://www.futuretimeline.net/forum/forum/" in url.get("href","") or "https://www.futuretimeline.net/forum/topic/" in url.get("href",""):
                url["href"] = url["href"].replace("https://www.futuretimeline.net/forum/forum/","../../../forum/forum/")
                url["href"] = url["href"].replace("https://www.futuretimeline.net/forum/topic/","../../../forum/topic/")
                #For technical reasons, the first page of a forum/topic is stored at /page-1 in this archive.
                #This is not so in the original forum, so has to be accounted for.           
                if "/page-" not in url["href"]:
                    url["href"] = url["href"][:url["href"].rindex("/")]+"/page-1"
        #Convert the html to a string and write it to the corresponding file
        thread_text = str(soup)
        with open(thread_url[31:]+"page-%d"%i,"w") as thread:
            print(thread_text,file=thread) 
        print(thread_url, "page %d out of %d processed"%(i,page_count))
        #exit loop at last page       
        if i == page_count:
            break

#This method parses an entire subforum in much the same way as the above method parses a single thread
def parse_subforum(subforum_url):
    try:
        os.mkdir("forum/forum/"+subforum_url.split("/")[-1])
    except:
        pass
    page_count = -1
    for i in range(1,9999999):

        subforum_text = requests.get(subforum_url+"/page-%d"%i).text
        soup = BeautifulSoup(subforum_text,features="html5lib")
        #List of all threads linked from this page of the subforum
        threads_on_page = list(map(lambda b:b.get("href"),list(filter(lambda a:"topic_title" in str(a),soup.find_all("a")))))
        if i == 1:        
            page_count = get_page_count(subforum_text)
        #this part works the same as in parse_thread
        urls = soup.find_all("a")
        for url in urls:

            if "https://www.futuretimeline.net/forum/forum/" in url["href"] or "https://www.futuretimeline.net/forum/topic/" in url["href"]:
                url["href"] = url["href"].replace("https://www.futuretimeline.net/forum/forum/","../../../forum/forum/")
                url["href"] = url["href"].replace("https://www.futuretimeline.net/forum/topic/","../../../forum/topic/")                
                if "/page-" not in url["href"]:
                    url["href"] = url["href"][:url["href"].rindex("/")]+"/page-1"
            if url["href"]=="https://www.futuretimeline.net/forum/":
                url["href"] = "../../index.html"
        subforum_text = str(soup)
        with open(subforum_url[31:]+"/page-%d"%i,"w") as subforum:
            print(subforum_text,file=subforum)

        #get every thread linked on the page and parse it
        for thread in threads_on_page:
            parse_thread(thread)
        print(subforum_url, "page %d out of %d processed"%(i,page_count))            
        if i == page_count:
            break

#Special method for parsing the home page
def parse_main_forum():
    main_forum_text = requests.get("https://www.futuretimeline.net/forum/").text
    soup = BeautifulSoup(main_forum_text,features="html5lib")
    urls = soup.find_all("a")
    #Replaces all futuretimeline links with links to this archive. No need to parse subforums or threads since those are done later.
    for url in urls:

        if "https://www.futuretimeline.net/forum/forum/" in url["href"] or "https://www.futuretimeline.net/forum/topic/" in url["href"]:
            url["href"] = url["href"].replace("https://www.futuretimeline.net/forum/forum/","./forum/")
            url["href"] = url["href"].replace("https://www.futuretimeline.net/forum/topic/","./topic/")                
            if "/page-" not in url["href"]:
                url["href"] = url["href"][:url["href"].rindex("/")]+"/page-1"
        if url["href"]=="https://www.futuretimeline.net/forum/":
            url["href"] = "index.html"
    with open("forum/index.html","w") as main_forum:
        print(str(soup),file=main_forum)
    print("main forum processed")
    
#Does all the stuff
parse_main_forum()
subforums = ["https://www.futuretimeline.net/forum/forum/1-on-topic",
             "https://www.futuretimeline.net/forum/forum/2-in-the-news-current-events",
             "https://www.futuretimeline.net/forum/forum/3-off-topic",
             "https://www.futuretimeline.net/forum/forum/4-website",
             "https://www.futuretimeline.net/forum/forum/6-science-technology-of-the-future",
             "https://www.futuretimeline.net/forum/forum/7-culture-economics-politics-of-the-future",
             "https://www.futuretimeline.net/forum/forum/8-images-and-videos",
             "https://www.futuretimeline.net/forum/forum/9-fictional-future",
             "https://www.futuretimeline.net/forum/forum/10-off-topic-general-discussion",
             "https://www.futuretimeline.net/forum/forum/11-news-announcements",
             "https://www.futuretimeline.net/forum/forum/12-introductions",
             "https://www.futuretimeline.net/forum/forum/13-feedback-suggestions",
             "https://www.futuretimeline.net/forum/forum/16-the-history-forum"]
for subforum in subforums:
    parse_subforum(subforum)
