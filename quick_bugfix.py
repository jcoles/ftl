#Quick fix for bug where links from threads to home page were pointing to original home page, not archived home page
import os
from bs4 import BeautifulSoup
for f in os.listdir("forum/topic"): #get every subforum 
    for f2 in os.listdir("forum/topic/"+f): #get every thread
        with open("forum/topic/%s/%s"%(f,f2),"r") as file_:
            thread_text = file_.read()
        soup = BeautifulSoup(thread_text)
        for url in soup.find_all("a"): #get all urls on page
            #replace link to forum homepage with link to archived homepage
            if url.get("href","")=="https://www.futuretimeline.net/forum/":
                url["href"] = "../../index.html"
        #write back to original file
        with open("forum/topic/%s/%s"%(f,f2),"w") as file_:
            print(str(soup),file=file_)
        print("processed %s, %s"%(f,f2))
